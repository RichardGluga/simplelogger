﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SimpleLogger {
    [DataContractAttribute]
    public class ClassLogLevel {
        [DataMemberAttribute]
        public string ClassName;
        [DataMemberAttribute]
        public LogLevel LogLevel;

        public ClassLogLevel(String resource, LogLevel level) {
            this.ClassName = resource;
            this.LogLevel = level;
        }
    }
}
