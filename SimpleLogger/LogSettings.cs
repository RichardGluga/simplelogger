﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace SimpleLogger {
    [DataContractAttribute]
    public class LogSettings {
        private static LogLevel defaultMaxLogLevel = LogLevel.DEBUG;
        private static LogLevel defaultDefaultLogLevel = LogLevel.DEBUG;
        private static bool defaultLogToConsole = true;
        private static String defaultLogToFile = "";
        private static bool defaultLogToWindowsEvents = true;
        private static String settingsFile = "SimpleLogger.xml";

        public static LogSettings settings;

        static LogSettings (){
            //static constructor ensures settings file is loaded first time the class is accessed
            LogSettings.load();
        }

        public LogSettings() {
            init();
        }

        public void init() {
            if (LogSettings.settings != null) LogSettings.settings.destroy();
            watchFile();
        }

        [DataMemberAttribute] private LogLevel maxLogLevel;
        [DataMemberAttribute] private LogLevel defaultLogLevel;
        [DataMemberAttribute] private bool logToConsole;
        [DataMemberAttribute] private String logToFile;
        [DataMemberAttribute] private bool logToWindowsEvents;
        [DataMemberAttribute] private ClassLogLevel[] ClassLogLevels;
        [DataMemberAttribute] private ClassMethodLogLevel[] ClassMethodLogLevels;

        public LogLevel DefaultLogLevel {
            get { return (LogLevel)defaultLogLevel; }
            set { defaultLogLevel = value; }
        }
        public String LogToFile {
            get { return logToFile; }
            set { logToFile = value; }
        }
        public bool LogToConsole {
            get { return logToConsole; }
            set { logToConsole = value; }
        }
        public LogLevel MaxLogLevel {
            get { return (LogLevel)maxLogLevel; }
            set { maxLogLevel = value; }
        }

        public bool LogToWindowsEvents {
            get { return logToWindowsEvents; }
            set { logToWindowsEvents = value; }
        }

        [OnDeserializing]
        void OnDeserialized(StreamingContext context) {
            resetDefaults();
        }

        public void resetDefaults() {
            defaultLogLevel = defaultDefaultLogLevel;
            maxLogLevel = defaultMaxLogLevel;
            logToConsole = defaultLogToConsole;
            logToFile = defaultLogToFile;
            logToWindowsEvents = defaultLogToWindowsEvents;

            ClassLogLevels = new ClassLogLevel[0];
            addClassLogLevel(new ClassLogLevel("ExampleClass", LogLevel.DEBUG));

            ClassMethodLogLevels = new ClassMethodLogLevel[0];
            addClassMethodLogLevel(new ClassMethodLogLevel("ExampleClass.someMethod", LogLevel.TRACE));
        }

        private void addClassLogLevel(ClassLogLevel cll) {
            Array.Resize(ref ClassLogLevels, ClassLogLevels.Length + 1);
            ClassLogLevels[ClassLogLevels.Length - 1] = cll;
        }

        public LogLevel getClassLogLevel(String className) {
            foreach (ClassLogLevel cll in ClassLogLevels) { 
                if(cll.ClassName.ToLower().Trim().Equals(className.ToLower().Trim())) return cll.LogLevel;
            }
            return LogLevel.UNDEFINED;
        }
        private void addClassMethodLogLevel(ClassMethodLogLevel cmll) {
            Array.Resize(ref ClassMethodLogLevels, ClassMethodLogLevels.Length + 1);
            ClassMethodLogLevels[ClassMethodLogLevels.Length - 1] = cmll;
        }

        public LogLevel getClassMethodLogLevel(String classMethodName) {
            foreach (ClassMethodLogLevel cmll in ClassMethodLogLevels) {
                if (cmll.ClassMethodName.ToLower().Trim().Equals(classMethodName.ToLower().Trim())) return cmll.LogLevel;
            }
            return LogLevel.UNDEFINED;
        }



        private FileSystemWatcher watcher;
        private void watchFile() {
            watcher = new FileSystemWatcher();
            watcher.Path = System.AppDomain.CurrentDomain.BaseDirectory;
            watcher.NotifyFilter = NotifyFilters.LastWrite;
            watcher.Filter = settingsFile;
            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.EnableRaisingEvents = true;
        }
        public void destroy() {
            if (watcher != null && watcher.EnableRaisingEvents) {
                watcher.EnableRaisingEvents = false;
            }
        }

        private void OnChanged(object source, FileSystemEventArgs e) {
            LogSettings.load();
            watcher.EnableRaisingEvents = false;
        }

        public static void save() {
            try {
                var serializer = new DataContractSerializer(typeof(LogSettings));
                String xmlString;
                using (var sw = new StringWriter()) {
                    using (var writer = new XmlTextWriter(sw)) {
                        writer.Formatting = Formatting.Indented; // indent the Xml so it's human readable
                        serializer.WriteObject(writer, LogSettings.settings);
                        writer.Flush();
                        xmlString = sw.ToString();
                    }
                }

                StreamWriter fw = File.CreateText(settingsFile);
                fw.Write(xmlString);
                fw.Flush();
                fw.Close();
            } catch (Exception ex) { 
                
            }
        }

        public static void load() {
            try {
                FileStream fs = new FileStream(settingsFile, FileMode.Open);
                XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
                DataContractSerializer ser = new DataContractSerializer(typeof(LogSettings));
                LogSettings s = (LogSettings)ser.ReadObject(reader, true);
                reader.Close();
                fs.Close();
                LogSettings.settings = s;
                LogSettings.settings.init();
            } catch (Exception) {
                LogSettings.settings = new LogSettings();
                LogSettings.settings.init();
                LogSettings.settings.resetDefaults();
                if (!File.Exists(settingsFile)) LogSettings.save();
            }
        }
    }
}
