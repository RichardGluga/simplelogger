﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;

namespace SimpleLogger {
    public enum LogLevel {
        UNDEFINED = 0,
        CRITICAL = 1,
        ERROR = 2,
        WARNING = 3,
        INFO = 4,
        DEBUG = 5,
        TRACE = 6
    };

    public class Log {
        public static event EventHandler<LogMessageEventArgs> logListener;
        private static void logMessage(LogLevel level, String message, string callerFilePath = "", string callerMemberName = "") {
            lock (LogSettings.settings) {
                String className = callerFilePathToClassName(callerFilePath);
                String methodName = callerMemberName;

                if (LogSettings.settings.MaxLogLevel >= level) {
                    LogLevel callerLogLevel = LogSettings.settings.getClassMethodLogLevel(className + "." + methodName);
                    if (callerLogLevel == LogLevel.UNDEFINED) callerLogLevel = LogSettings.settings.getClassLogLevel(className);
                    if (callerLogLevel == LogLevel.UNDEFINED) callerLogLevel = LogSettings.settings.DefaultLogLevel;

                    if (callerLogLevel >= level) {
                        String type = "";
                        if (level == LogLevel.CRITICAL) type = "CRITICAL";
                        if (level == LogLevel.ERROR) type = "ERROR";
                        if (level == LogLevel.WARNING) type = "WARNING";
                        if (level == LogLevel.INFO) type = "INFO";
                        if (level == LogLevel.DEBUG) type = "DEBUG";
                        if (level == LogLevel.TRACE) type = "TRACE";

                        LogMessageEventArgs log = new LogMessageEventArgs(className, methodName, message, (LogLevel)callerLogLevel, type);

                        if (LogSettings.settings.LogToConsole) System.Console.WriteLine(log);
                        if (LogSettings.settings.LogToWindowsEvents) logToWindowsEvents(log);
                        if (LogSettings.settings.LogToFile != null && LogSettings.settings.LogToFile.Trim().Length > 0)  logToFile(log);

                        if (logListener != null) logListener(null, log);
                    }
                }
            }
        }

        public static void critical(String message = "", [CallerFilePath] string callerFilePath = "", [CallerMemberName] string callerMemberName = "") {
            logMessage(LogLevel.CRITICAL, message, callerFilePath, callerMemberName);
        }

        public static void error(String message = "", [CallerFilePath] string callerFilePath = "", [CallerMemberName] string callerMemberName = "") {
            logMessage(LogLevel.ERROR, message, callerFilePath, callerMemberName);
        }
        public static void warn(String message = "", [CallerFilePath] string callerFilePath = "", [CallerMemberName] string callerMemberName = "") {
            logMessage(LogLevel.WARNING, message, callerFilePath, callerMemberName);
        }

        public static void info(String message = "", [CallerFilePath] string callerFilePath = "", [CallerMemberName] string callerMemberName = "") {
            logMessage(LogLevel.INFO, message, callerFilePath, callerMemberName);
        }
        public static void debug(String message = "", [CallerFilePath] string callerFilePath = "", [CallerMemberName] string callerMemberName = "") {
            logMessage(LogLevel.DEBUG, message, callerFilePath, callerMemberName);
        }
        public static void trace(String message = "", [CallerFilePath] string callerFilePath = "", [CallerMemberName] string callerMemberName = "") {
            logMessage(LogLevel.TRACE, message, callerFilePath, callerMemberName);
        }

        private static void logToFile(LogMessageEventArgs log) {
            try {
                using (StreamWriter w = File.AppendText(LogSettings.settings.LogToFile)) {
                    w.WriteLine(log);
                }
            } catch (Exception ex) {
                LogSettings.settings.LogToFile = null;
                Log.error("Could not log to file - " + ex.Message);
            }
        }

        public static string ProductName {
            get {
                AssemblyProductAttribute myProduct = (AssemblyProductAttribute)AssemblyProductAttribute.GetCustomAttribute(Assembly.GetExecutingAssembly(),typeof(AssemblyProductAttribute));
                return myProduct.Product;
            }
        }

        private static void logToWindowsEvents(LogMessageEventArgs log) {
            try {
                EventLog e = new EventLog();
                e.Source = "AppEvent";

                EventLogEntryType type = EventLogEntryType.Information;
                if (log.LogLevel >= LogLevel.ERROR) type = EventLogEntryType.Error;
                else if (log.LogLevel == LogLevel.WARNING) type = EventLogEntryType.Warning;
                else if (log.LogLevel == LogLevel.INFO) type = EventLogEntryType.Information;
                else return;

                e.WriteEntry(log.ToString(), type);
            } catch (Exception ex) {
                LogSettings.settings.LogToWindowsEvents = false;
                Log.error("Could not log to windows event log. Disabling future attempts. Error message is - " + ex.Message);
            }
        }

        private static string callerFilePathToClassName(string callerFilePath) {
            string className = "Unknown";

            if (callerFilePath != null && callerFilePath.Trim().Length > 0) {
                string[] fields = callerFilePath.Split('.');
                if (fields.Length > 1) {
                    string path = fields[fields.Length - 2];
                    fields = path.Split('\\');
                    if(fields.Length > 1) className = fields[fields.Length - 2];
                }
            }

            return className;
        }
    }
}
