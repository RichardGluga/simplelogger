﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SimpleLogger {
    [DataContractAttribute]
    public class ClassMethodLogLevel {
        [DataMemberAttribute]
        public string ClassMethodName;
        [DataMemberAttribute]
        public LogLevel LogLevel;

        public ClassMethodLogLevel(String resource, LogLevel level) {
            this.ClassMethodName = resource;
            this.LogLevel = level;
        }
    }
}
