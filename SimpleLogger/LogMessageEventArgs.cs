﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleLogger {
    public class LogMessageEventArgs : EventArgs {
        private DateTime logTime = DateTime.MinValue;

        private String callerClass;

        public String CallerClass {
            get { return callerClass; }
            set { callerClass = value; }
        }
        private String callerMethod;

        public String CallerMethod {
            get { return callerMethod; }
            set { callerMethod = value; }
        }
        private String message;

        public String Message {
            get { return message; }
            set { message = value; }
        }
        private LogLevel logLevel;

        public LogLevel LogLevel {
            get { return logLevel; }
            set { logLevel = value; }
        }
        private String logLevelString;

        public String LogLevelString {
            get { return logLevelString; }
            set { logLevelString = value; }
        }
        
        public LogMessageEventArgs(String callerClass, String callerMethod, String message, LogLevel logLevel, String logLevelString) {
            this.callerClass = callerClass;
            this.callerMethod = callerMethod;
            this.message = message;
            this.logLevel = logLevel;
            this.logLevelString = logLevelString;
            this.logTime = DateTime.Now;
        }

        public override String ToString() {
            return "[" + logTime.ToShortDateString() + " " + logTime.ToLongTimeString() + "][" + logLevelString + "][" + callerClass + "." + callerMethod + "][" + message + "]"; 
        }


    }
}
